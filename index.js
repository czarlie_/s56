function countLetter(letter, sentence) {
  let result = 0;
  let sentenceLowerCase = sentence.toLowerCase();

  if (letter.length === 1) {
    for (let i = 0; i < sentenceLowerCase.length; i++) {
      if (letter === sentenceLowerCase[i]) result += 1;
    }
  } else return undefined;

  console.log(result);
}

// countLetter("i", "The quick brown fox jumps over the lazy dog");

function isIsogram(text) {
  let newText = text.toLowerCase();
  for (let i = 0; newText[i]; i++) {
    for (let j = 0; newText[i]; j++) {
      return newText[i] === newText[j] ? false : true;
    }
  }

  // let textLowerCase = text.toLowerCase();

  // if (letter.length === 1) {
  //   for (let i = 0; i < textLowerCase.length; i++) {
  //     if (letter === textLowerCase[i]) result += 1;
  //   }
  // } else return undefined;

  // console.log(result);

  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  // If the function finds a repeating letter, return false. Otherwise, return true.
}
//console.log(isIsogram("fivee"));

function purchase(age, price) {
  let discount = price * 0.2;
  if (age < 14) return undefined;
  if ((age > 64 && age > 12) || age < 22)
    return (price - discount).toFixed(2).toString();
  else if (age >= 22 && age <= 64) return price.toFixed(2).toString();

  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  // Return the rounded off price for people aged 22 to 64.
  // The returned value should be a string.
}

console.log(purchase(65, 109.4356));

function findHotCategories(items) {
  for (item of items) {
    if (item.stocks === 0) {
      return item.category;
    }
  }
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
}

function findFlyingVoters(candidateA, candidateB) {
  candidateA.forEach(testChecker());

  function testChecker(value) {
    value === candidateB.forEach();
  }

  //for (candidateA of candidate)
  // Find voters who voted for both candidate A and candidate B.
  // The passed values from the test are the following:
  // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
